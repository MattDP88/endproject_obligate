package be.matthiasdepoorter.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import be.matthiasdepoorter.domain.beans.Obligator;
import be.matthiasdepoorter.domain.interfaces.ObligatorRepository;

@Controller()
@RequestMapping("save")
public class SaveController {
	
	@Autowired
	private ObligatorRepository repo;
	
	@GetMapping
	public String saveObject(){
		Obligator amin = new Obligator();
		amin.setFirstName("Matthias");
		amin.setLastName("De Poorter");
		amin.setPassword("intec-123");
		amin.setInterests("test, hello, whatever");
		repo.save(amin);
		return "test";
	}

}
