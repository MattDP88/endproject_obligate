package be.matthiasdepoorter.domain.beans;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document
public class Task{
	@Id
	private String objectId;
	private String name;
	private String type;
	private boolean finished;
	
	public Task(){
		
	}

	public Task(String name, String type, boolean finished) {
		super();
		this.name = name;
		this.type = type;
		this.finished = finished;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isFinished() {
		return finished;
	}

	public void setFinished(boolean finished) {
		this.finished = finished;
	}
	@Override
	public String toString() {
		return "Task [objectId= " + objectId+ ", name=" + name + ", type=" + type + ", finished=" + finished + "]";
	}

}
