package be.matthiasdepoorter.domain.beans;

import java.util.List;

import org.mongodb.morphia.annotations.Embedded;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Reference;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="obligators")
public class Obligator {

	@Id
	private String objectId;
	private String userName;
	private String password;
	private String firstName;
	private String lastName;
	private String[] interests;
	@Embedded(concreteClass = Task.class)
	private List<Task> taskList;
	@Reference
	private Obligator linkedUser;

	public Obligator() {

	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String[] getInterests() {
		return interests;
	}

	public void setInterests(String... interests) {
		this.interests = interests;
	}

	public List<Task> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Task> taskList) {
		this.taskList = taskList;
	}

	public Obligator getLinkedUser() {
		return linkedUser;
	}

	public void setLinkedUser(Obligator linkedUser) {
		this.linkedUser = linkedUser;
	}

	@Override
	public String toString() {
		return "User [objectId=" + objectId + ", userName=" + userName + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + "]";
	}

}
