package be.matthiasdepoorter.domain.interfaces;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import be.matthiasdepoorter.domain.beans.Obligator;

@RepositoryRestResource(path="obligators", collectionResourceRel="obligators")
public interface ObligatorRepository extends MongoRepository <Obligator, String>{
	
	public Obligator findByUserName(@Param("userName") String userName);
	
	//@Query("{'interest' : ?0}")
	public List<Obligator> findByInterests(@Param("interest") String interest);
	
	
	
}
