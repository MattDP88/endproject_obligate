package be.matthiasdepoorter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.core.MongoTemplate;

import be.matthiasdepoorter.domain.beans.Obligator;
import be.matthiasdepoorter.domain.interfaces.ObligatorRepository;

@SpringBootApplication
@ComponentScan(basePackages = "be.matthiasdepoorter")
public class SpringApp {
	
	public static void main (String [] args){
		SpringApplication.run(SpringApp.class, args);
		
	}

}
