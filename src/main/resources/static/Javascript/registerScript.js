/**
 * Created by MatthiasD on 1/03/2017.
 */

var form;
var data;

function handleForm(e) {
	e.preventDefault();
    form = document.getElementById("registrationForm");
    data = {};
    for (var i = 0, ii = form.length; i < ii; ++i) {
        var input = form[i];
        if (input.name) {
            data[input.name] = input.value;

        }
    }

    addData();
}
function addData() {

    $.ajax({
        type: "POST",
        url: "http://localhost:8080/obligators",
        contentType: "application/json",
        data: JSON.stringify(data),
        dataType: "json",
        crossDomain : true,

        success: function(){
        	window.location.href="success.html";
        },
        error: function(){
        }
    });
}

